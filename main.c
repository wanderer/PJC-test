#include <stdio.h>
#include <stdlib.h>

int get_user_input(){
	int x = -1;
	int ok = -1;
	while( ok != 1){
		/* as array values will only be >0, unsigned int is fine */
		ok = scanf("%u", &x);
		if(ok == EOF){
			printf("\ncaught Ctrl+D...\n");
			ok = 1;
			x = 2;
			break;
		}
		if(ok == 0){
			while (fgetc(stdin) != '\n'){
			}
			printf("[*] give me a correct value\n");
			break;
		}
		}
	return x;
}

int lemmego(){
	printf("[*] you wished to go, quitting...\n");
	return EXIT_SUCCESS;
}

void print_banner(){
	printf("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n"
		"|u|l|t|i|m|a|t|e|_|a|r|r|a|y|_|t|r|a|n|s|l|a|t|o|r|\n"
		"+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n");
}

void do_magic(uint arr_len){
	printf("[i] arr_len = %u\n", arr_len);
	int *noice_arr = malloc(arr_len*sizeof(int));

	printf("noice_arr: [ ");
	for (int i = 0; i < arr_len; i++){
		noice_arr[i] = i;
		printf("%d ", noice_arr[i]);
	}
	printf("]\n");

	printf("[*] translating the array\n");

	int *ugly_arr = malloc(arr_len*sizeof(int));
	uint helper = arr_len-1;
	printf("ugly_arr: [ ");
	for (int i = 0; i < arr_len; i++, helper--){
		ugly_arr[i] = noice_arr[helper];
		printf("%d ", ugly_arr[i]);
	}
	printf("]\n");

	free(noice_arr);
	free(ugly_arr);
}

int main()
{
	int selection=0, arr_len=0;

	print_banner();

	while ( selection != -1 ) {
		printf("\npick a choice\n");
		printf("9000 - do magic\n"
		"1 - quit\n"
		"~> ");

	selection = get_user_input();

	switch ( selection ) {
		case 9000:
			printf("[*] please gimme a natural number:\n"
			"--> ");
			arr_len = get_user_input();
			if (arr_len == 0) {
				printf("[!] array cannot have zero size, mate...\n");
				break;
			}
			printf("[*] doing evil stuff\n");
			do_magic(arr_len);
			break;
		case 1:
			lemmego();
			return 0;
		default:
			printf("[x] invalid option\n");
			selection = 0;
			break;
		}
	}
	return 0;
}
